from django.conf.urls import include, url
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib.auth import views as log_views
from common import views as common
from django.contrib.auth import  views as auth_views



urlpatterns = [
    url(r'^', include('users.urls', namespace='users')),
    url(r'^panel', common.panel),
    url(r'^reservas', common.reservas),
    url(r'^recursos/', include('recursos.urls', namespace='recursos')),
    url(r'^admin/', admin.site.urls),
    url(r'^reserva_generica/', include('reserva_generica.urls',namespace='reserva_generica')),
    url(r'^logout/', log_views.logout,name='logout'),
    url(r'^$', common.start),

    url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),
]
urlpatterns += staticfiles_urlpatterns()
