from django import template
from django.template.defaulttags import token_kwargs, TemplateSyntaxError, WithNode

from django.utils.html import conditional_escape
from django.utils.safestring import mark_safe
from django.urls import reverse
from django.utils import six


class NavBar(template.Node):
    def __init__(self, nodelist, extra_context):
        self.nodes = nodelist
        self.extra_context = extra_context

    def render(self, context):
        t = template.Template("""<ul class="{{classes}} id={{id}}"> {{content}} </ul>""")
        values = {key: val.resolve(context) for key, val in six.iteritems(self.extra_context)}
        local_context = {
            'id': values.get('id', ""),
            'classes': "nav %s" % (values.get('class', ""),),
            'content': self.nodes.render(context)
        }
        with context.push(**local_context):
            return mark_safe(t.render(context))


register = template.Library()


@register.filter(needs_autoescape=True)
def ejemplo(text, autoescape=True):
    first, other = text[0], text[1:]
    if autoescape:
        esc = conditional_escape
    else:
        esc = lambda x: x
    result = '<strong>%s</strong>%s' % (esc(first), esc(other))
    return mark_safe(result)

@register.inclusion_tag("navitem.html")
def navitem(link, name):
    return {"link": link, "name": name}

register.filter('as_url', reverse)

@register.tag
def navbar(parser, token):
    bits = token.split_contents()
    remaining_bits = bits[1:]
    extra_context = token_kwargs(remaining_bits, parser) or {}
    if remaining_bits:
        raise TemplateSyntaxError("%r received an invalid token: %r" %
                                  (bits[0], remaining_bits[0]))
    nodelist = parser.parse(('endnavbar',))
    parser.delete_first_token()
    return NavBar(nodelist, extra_context=extra_context)
