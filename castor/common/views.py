from django.shortcuts import Http404
from . import decorators


def panel(request):
    return Http404()


def reservas(request):
    return Http404()


@decorators.redirect_on_group('admin', '/panel')
@decorators.redirect_on_group('users', '/reservas')
def start(request):
    return Http404()


