from functools import wraps
from django.shortcuts import redirect


# checkRole :: (Request -> ...args -> Response) -> Role -> (Request -> ...args -> Response)
def check_role(predicate, view_fn):
    return lambda request, *args, **kwargs: view_fn(request, *args, **kwargs) if predicate(request) else {"status_code": 403}


# checkPredicate ::  ( a -> Bool) -> (a -> b) -> ( a->c)
def if_else(predicate, yesfn, nofn):
    return lambda argument: yesfn(argument) if predicate(argument) else nofn(argument)


def redirect_on_group(group_name, redirection):
    """
    Decorador que recibe un nombre de grupo y envuelve a la función decorada
    de manera que emita una redirección apropiada
    :param group_name: el nombre del grupo que debe satisfacer
    :param redirection: la url a la que debe direccionar de ser del grupo mencionado
    :return: 
    """
    def _check_group(view_func):
        @wraps(view_func)
        def wrapper(request, *args, **kwargs):
            #si el usuario no está registrado redireccionar a login
            print(group_name)
            if request.user.is_anonymous:
                return redirect('/login')
            if request.user.groups.filter(name=group_name).exists() or request.user.is_superuser:
                return redirect(redirection)
            return view_func(request, *args, **kwargs)
        return wrapper
    return _check_group