from django.conf.urls import  url
from . import views
from .views import *

urlpatterns = [
  url(r'^$', views.IndexView.as_view(), name='index'),
  url(r'^(?P<pk>[0-9]+)/$', views.DetailView.as_view(), name='detail'),

  url(r'tiporecurso/add/$', views.CrearTipo.as_view(), name='agregar-tiporecurso'),
  url(r'tiporecurso/(?P<pk>[0-9]+)/$', tiporecurso_update, name='actualizar-tiporecurso'),
  url(r'tiporecurso/(?P<pk>[0-9]+)/eliminar/$', views.EliminarTipo.as_view(), name='eliminar-tiporecurso'),

  url(r'^edit/(?P<pk>\d+)$', recurso_update, name='recurso_edit'),
  url(r'^lista/$', recurso_list, name='recurso_list'),
  url(r'^new/$', recurso_create, name='recurso_new'),
  url(r'^delete/(?P<pk>\d+)$', views.recursoDelete.as_view(), name='recurso_delete'),
  url(r'^tiporecurso/recursos/(?P<pk>\d+)$', views.eliminarRecurso.as_view(), name='eliminar_recurso'),
  url(r'^busqueda/recursos/$', recurso_busqueda , name='recurso_busqueda'),
  url(r'^busqueda/tipo_recurso/$', tipo_recurso_busqueda , name='tipo_recurso_busqueda'),
  url(r'^estados/$', recurso_estados, name='recurso_estados'),


]

