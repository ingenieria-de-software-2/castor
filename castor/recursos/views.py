from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect, get_object_or_404
from django.forms import ModelForm
from django.views import generic
from django.views.generic.edit import CreateView,UpdateView,DeleteView
from django.core.urlresolvers import reverse_lazy
from .models import TipoRecurso,Recurso

#**handle_POST**
"""
retorna una funcion, si el metodo del request es POST
"""
def handle_POST(request, fn):
    if (request.method == 'POST'):
        return fn(request)
    return None

#**handle_GET**
"""
retorna una funcion, si el metodo del request es POST
"""
def handle_GET(request, fn):
    if (request.method == 'GET'):
        return fn(request)
    return None


class IndexView(generic.ListView):
    template_name = 'recursos/index.html'
    context_object_name = 'lista'

    #**get_queryset**
    """
    Lista tipo de recursos existentes
    """
    def get_queryset(self):
        return TipoRecurso.objects.all()




#**Class DetailView**
"""
Agrega soporte para comparar con el signo ==
"""
class DetailView(generic.DetailView):
    model = TipoRecurso
    template_name = 'recursos/detalle.html'

#**Class CrearTipo**
"""
Crea un tipo de recurso con sus caracteristicas
"""
class CrearTipo(CreateView):
    model = TipoRecurso
    fields = ['Nombre','caracteristicas']

#**Class ModificarTipo**
"""
Modifica tipo de Recurso
"""
class ModificarTipo(UpdateView):
    model = TipoRecurso
    fields = ['Nombre','caracteristicas']

#**Class TipoRecursoForm**
"""
Crea el formulario utilizado en TipoRecurso
"""
class TipoRecursoForm(ModelForm):
    class Meta:
        model = TipoRecurso
        fields = ['Nombre', 'caracteristicas']


#**tiporecurso_update**
"""
guarda las modificaciones del TipoRecurso seleccionado
"""
def tiporecurso_update(request, pk, template_name='recursos/tiporecurso_form.html'):
    Tiporecurso = get_object_or_404(TipoRecurso, pk=pk)
    form = TipoRecursoForm(request.POST or None, instance=Tiporecurso)
    if form.is_valid():
        form.save()
        print(form.instance)
        return redirect('recursos:index')
    return render(request, template_name, {'form':form})

#**EliminarTipo**
"""
Elimina el TipoRecurso seleccionado
"""
class EliminarTipo(DeleteView):
    model = TipoRecurso
    fields = ['Nombre','caracteristicas']
    success_url = reverse_lazy('recursos:index')

#**Class RecursoForm**
"""
Crea la clase Recurso
"""
class RecursoForm(ModelForm):
    class Meta:
        model = Recurso
        fields = ['tipo', 'nombre', 'estado', 'cantidadHabilitada', 'TiempoEstimadoVida']

#**recurso_list**
"""
Lista los recursos existentes
"""

def recurso_list(request):
    """
    """
    result = {
        'object_list': handle_GET(request, search_recursos),

    }
    return render(request, 'recursos/recurso_list.html' , result)

#**recurso_create**
"""
Crea recurso y le asigna un tipo de recurso
"""
@login_required(login_url='/login/')
def recurso_create(request, template_name='recursos/recurso_form.html'):
    form = RecursoForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect(form.instance.tipo)
    return render(request, template_name, {'form': form})

#**recurso_update**
"""
modifica el recurso seleccionado
"""
@login_required(login_url='/login/')
def recurso_update(request, pk, template_name='recursos/recurso_form.html'):
    recurso = get_object_or_404(Recurso, pk=pk)
    form = RecursoForm(request.POST or None, instance=recurso)
    if form.is_valid():
        form.save()
        print(form.instance)
        return redirect('recursos:recurso_list')
    return render(request, template_name, {'form':form})

#**Class recursoDelete**
"""
Elimina el Recurso seleccionado y direcciona al listado de recursos
"""
class recursoDelete(DeleteView):
    model = Recurso
    fields = ['tipo', 'nombre', 'estado', 'cantidadHabilitada', 'cantidadHabilitada', 'cantidadDisponible',
              'TiempoEstimadoVida']
    success_url = reverse_lazy('recursos:recurso_list')

#**Class eliminarRecurso**
"""
Elimina el Recurso seleccionado y direcciona a la pagina index
"""
class eliminarRecurso(DeleteView):
    model = Recurso
    fields = ['tipo', 'nombre', 'estado', 'cantidadHabilitada', 'cantidadHabilitada', 'cantidadDisponible',
              'TiempoEstimadoVida']
    success_url = reverse_lazy('recursos:index')

#**recurso_busqueda**
"""
Realiza una busqueda del recurso solicitado
"""
@login_required(login_url='/login/')
def recurso_busqueda(request, template_name='recursos/mostrar_recurso_list.html'):

    miNombre = request.GET['q']
    recursos = Recurso.objects.all().filter(nombre=miNombre)
    data = {}
    data['object_list'] = recursos
    return render(request, template_name, data)

#**tipo_recurso_busqueda**
"""
Buscamos todos los recursos que en el campo nombre sean iguales al nombre dado y los listamos
"""
def tipo_recurso_busqueda(request, template_name='recursos/mostrar_tipo_recurso_list.html'):
    print(request.GET  )
    miNombre = request.GET['q']
    print(miNombre)
    tipo_recurso = TipoRecurso.objects.all().filter(Nombre=miNombre)
    data = {}
    if request.GET['q']  is None :
        tipo_recurso = TipoRecurso.objects.all()
    data['object_list'] = tipo_recurso
    return render(request, template_name, data)

#**recurso_estados**
"""
Genera el estado de todos los recursos
"""
@login_required(login_url='/login/')
def recurso_estados(request, template_name='recursos/recurso_estados.html'):
    recurso = Recurso.objects.all()
    data = {}
    data['object_list'] = recurso
    return render(request, template_name, data)

#**search_recursos**
"""
busqueda del recurso solicitado segun el campo seleccionado
"""
@login_required(login_url='/login/')
def search_recursos(request):
    criteria = {}
    field=request.GET.get('field', None)
    if field is None:
        return Recurso.objects.all()
    criteria[field+"__iexact"] = request.GET.get('q', "")
    return Recurso.objects.filter(**criteria)

#**search_TipoRecurso**
"""
busqueda del TipoRecurso solicitado segun el campo seleccionado
"""
@login_required(login_url='/login/')
def search_TipoRecurso(request):
    criteria = {}
    field=request.GET.get('field', None)
    if field is None:
        return TipoRecurso.objects.all()
    criteria[field+"__iexact"] = request.GET.get('q', "")
    return TipoRecurso.objects.filter(**criteria)



