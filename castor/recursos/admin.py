# Register your models here.
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from .models import TipoRecurso, Recurso

admin.site.register(TipoRecurso)
admin.site.register(Recurso)

