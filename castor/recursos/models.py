from django.core.urlresolvers import reverse
from django.db import models
import datetime
from django.utils.timezone import now

STATUS_CHOICES = (
    (1, "Disponible"),
    (2, "Reservado"),
    (3, "Mantenimiento"),
    (4, "Inhabilitado"),
)

#**Class TipoRecurso**
"""
Crea la clase TipoRecurso con sus atributos
"""
class TipoRecurso(models.Model):
    Nombre = models.CharField(max_length=100)
    caracteristicas = models.CharField(max_length=1000)

    # **get_absolute_url**
    """
    retorna el id del recurso, dentro de la URL
    """
    def get_absolute_url(self):
        return reverse('recursos:detail', kwargs={'pk': self.pk})

    # **__str__**
    """
    retorna el nombre del recurso
    """
    def __str__(self):
        return self.Nombre

#**setDisponibles**
"""
retorna la cantidad disponible de recursos
"""
def setDisponibles(self):
    return self.cantidadHabilitada

#**Class Recurso**
"""
Crea la Clase recurso con sus atributos
"""
class Recurso(models.Model):
    tipo = models.ForeignKey(TipoRecurso, on_delete=models.CASCADE)
    nombre = models.CharField(max_length=50)
    estado = models.IntegerField(choices=STATUS_CHOICES, default=1)
    cantidadHabilitada = models.PositiveIntegerField(null=True)
    cantidadReservada = models.PositiveIntegerField(default=0)
    cantidadDisponible = models.PositiveIntegerField(null=True)
    fecha_creacion = models.DateField(default=now)
    TiempoEstimadoVida = models.PositiveIntegerField(default=100)

    # TODO: agregar una funcion on save, para setear el valor de cantidadDisponible

    def __str__(self):
        return self.nombre

    def save(self, *args, **kwargs):
        if not self.cantidadDisponible:
            self.cantidadDisponible = self.cantidadHabilitada
            super(Recurso, self).save(*args, **kwargs)
