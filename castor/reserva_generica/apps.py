from django.apps import AppConfig


class ReservaGenericaConfig(AppConfig):
    name = 'reserva_generica'
