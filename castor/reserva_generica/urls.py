from django.conf.urls import  url

from .views import *

urlpatterns = [
        url(r'^crear_reserva/$', crear_reserva, name='crear_reserva'),
        url(r'^modificar_reserva/(?P<pk>\d+)/$', Modificar_reserva.as_view() , name='modificar_reserva'),
        url(r'^listar_reserva/$', reserva_list , name='listar_reservas'),
        url(r'^cancelar_reserva/(?P<pk>\d+)$', Cancelar_reserva.as_view(), name='cancelar_reserva'),
]
