from django import forms
from django.forms import ModelForm

from .models import ReservaGenerica


class DateInput(forms.DateInput):
    input_type = 'date'

class TimeInput(forms.TimeInput):
    input_type = 'time'

class ReservaForm(ModelForm):

    class Meta:
        model = ReservaGenerica
        fields = ['tipoRecurso', 'fecha_inicio','hora_inicio','fecha_fin','hora_fin']
        widgets = {
            'fecha_inicio': DateInput(),
            'hora_inicio':TimeInput(),
            'fecha_fin':DateInput(),
            'hora_fin':TimeInput(),
        }

