from django.shortcuts import render, redirect, get_object_or_404

from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from .models import *
from .form import ReservaForm
from django.views.generic.edit import CreateView,UpdateView,DeleteView
from django.core.urlresolvers import reverse_lazy
# Create your views here.



# clase :crear_reserva
# metodo: post
# template : reserva_form
# post: datos{ ... falta definir modelos}
# request: /reserva_generica/crear_reserva/

def crear_reserva(request, template_name='reserva_generica/reserva_form.html'):
    if request.method == 'POST':
        form = ReservaForm(request.POST)

        if form.is_valid():
            post = form.save(commit=False)
            post.usuario = request.user
            post.save()
            return redirect('reserva_generica:listar_reservas')
        else:
            return redirect('reserva_generica:crear_reserva')
    else:
         form = ReservaForm()
    return render(request, template_name ,{ 'form' : form } )

# class Crear_reserva(CreateView):
#     model = ReservaGenerica
#     fields = ['tipoRecurso','fecha_inicio' , 'fecha_fin']
#     template_name = 'reserva_generica/reserva_form.html'
#     success_url = reverse_lazy('reserva_generica:listar_reservas')



    # reserva = ReservaForm(request.POST or None)
    # if reserva.is_valid():
    #     reserva.save()
    #     return redirect('reserva_generica:listar_reservas')
    # return render(request,template_name,  {'form': reserva})



    # clase: modificar_reserva
    # metodo: get , post
    # template: reserva_form
    # get datos = { ... falta definir modelos}
    # post datos
    # request: /reserva_generica/modificar_reserva/<pk>

class Modificar_reserva(UpdateView):
    model = ReservaGenerica
    fields = ['tipoRecurso','fecha_inicio' , 'fecha_fin']
    template_name = 'reserva_generica/reserva_form.html'
    success_url = reverse_lazy('reserva_generica:listar_reservas')

    # clase : generar_comprobante
    # metodo: get
    # template: comprobante.html
    # get { ... falta definir modelos}
    # request: /reserva_generica/comprobante/<pk>


    # clase: listar_reservas
    # metodo: get
    # template : lista_reservas.html
    # get { ... falta definir modelos}
    # request:  /reserva_generica/lista_de_reservas/
def reserva_list(request, template_name = 'reserva_generica/lista_reservas.html' ):
    reservas = ReservaGenerica.objects.all()
    data = {}
    data['object_list'] = reservas
    return render(request,template_name,data)

    # clase: cancelar_reserva
    # metodo: post
    # template: lista_reservas.html
    # post datos{ ... falta definir modelos }
    # request: /reserva_generica/cancelar_reserva/ ... XXX  falta definir XXX

class Cancelar_reserva(DeleteView):
    model = ReservaGenerica
    fiels = ['tipoRecurso','fecha_inicio' , 'fecha_fin']
    template_name = 'reserva_generica/lista_reservas.html'
    success_url = reverse_lazy('reserva_generica:listar_reservas')

    # clase: solicitud_enviada XXX falta definir XXX sera una clase o una funcion?
    # metodo: no recibe parametros
    # template: solicitud_enviada.html XXX solo seria texto XXX
    # request: /reserva_generica/solucitud_enviada/

