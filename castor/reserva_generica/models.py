from django.db import models
from users.models import Usuario , User
from recursos.models import TipoRecurso
from django.utils import timezone


#**Class ReservaGenerica**
"""
Crea Formulario de ReservaGenerica
"""
class ReservaGenerica(models.Model):
    usuario = models.ForeignKey(User, null=False)
    tipoRecurso = models.ForeignKey(TipoRecurso, null=False)
    fecha_inicio = models.DateTimeField(blank=False)
    hora_inicio=models.TimeField(default='00:00')
    fecha_fin = models.DateTimeField(blank=False)
    hora_fin = models.TimeField(default='00:00')
    fecha_fin = models.DateTimeField(blank=False)
