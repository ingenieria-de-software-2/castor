from django.db import models
from django.contrib.auth.models import User

STATUS_CHOICES = (
    (0,"No asignado"),
    (1, "Titular"),
    (2, "Adjunto"),
    (3, "Asistente"),
    (4, "Encargado"),
    (5, "Auxiliar"),
    (6, "Alumno"),
    (7, "Funcionario"),
)


#**Class Usuario**
"""
Crea la clase usuario con sus atributos
"""
class Usuario(User):
    ci = models.CharField(max_length=20)
    rol = models.IntegerField(null=True)
    categoria = models.IntegerField(choices=STATUS_CHOICES, default=0)
    direccion = models.CharField(max_length=100)
    telefono = models.CharField(max_length=20)
