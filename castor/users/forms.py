from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import Group
from .models import Usuario
from django.core.exceptions import ValidationError, ObjectDoesNotExist
from django_select2.forms import *

#**Class RegisterForm**
"""
Crea la clase de formulario de Registro de usuario con sus atributos
"""
class RegisterForm(UserCreationForm):
    first_name = forms.CharField(label="Nombre")
    last_name = forms.CharField(label="Apellido")
    email = forms.EmailField(label="Correo electronico")
    ci = forms.CharField(max_length=20, label="CI")
    telefono = forms.CharField(max_length=20, label="Telefono")
    direccion = forms.CharField(max_length=100, label="Direccion")

    class Meta:
        model = Usuario
        fields = ("ci", "username", "email", "first_name", "last_name", "telefono", "direccion", "password1",  "password2", )

    def __init__(self, *args, **kwargs):
        super(RegisterForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs.update({'class': 'form-control'})

    # **clean_email**
    """
    Verifica que el correo no este asignado a un usuario ya registrado
    """
    def clean_email(self):
        email = self.cleaned_data["email"]
        try:
            Usuario.objects.get(email=email)
        except ObjectDoesNotExist:
            return email
        raise ValidationError("A user with that email address already exists.")

    # **save**
    """
    guarda el usuario
    """
    def save(self, commit=True):
        user = super(UserCreationForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        user.is_active = False
        if commit:
            user.save()
        return user

#**Class GroupsForm**
"""
Crea la clase de formulario de roles de usuario con sus atributos
"""
class GroupsForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = ['name', 'permissions']
        widgets = {
                'permissions': Select2MultipleWidget
        }
    def __init__(self, *args, **kwargs):
        super(GroupsForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            if self.fields[field].widget.__class__.__name__ != 'CheckboxInput':
                self.fields[field].widget.attrs = {
                    'class': 'form-control'
                }

#**Class UserForm**
"""
Crea la clase de formulario de usuario con sus atributos
"""
class UserForm(forms.ModelForm):
    class Meta:
        model = Usuario
        fields = (
            'ci', 'username', 'first_name', 'last_name', 'email', 'direccion', 'telefono', 'password', 'categoria',
            'is_active',)

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            if self.fields[field].widget.__class__.__name__ != 'CheckboxInput':
                self.fields[field].widget.attrs = {
                    'class': 'form-control'
                }
