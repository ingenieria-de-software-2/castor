from django.shortcuts import render, HttpResponseRedirect, redirect, get_object_or_404
from django.contrib import messages

from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.conf import settings

from .models import Usuario
from .forms import RegisterForm, GroupsForm, UserForm
from django.contrib.auth.models import Group
from django.forms import ModelForm


#**handle_POST**
"""
Retorna una funcion si el metodo del request es POST
"""
def handle_POST(request, fn):
    if (request.method == 'POST'):
        return fn(request)
    return None

#**handle_POST**
"""
Retorna una funcion si el metodo del request es POST
"""
def handle_GET(request, fn):
    if (request.method == 'GET'):
        return fn(request)
    return None

#**notifyRegister**
"""
Notifica al usuario que su Registro fue exitoso
"""
def notifyRegister(request, email):
    subject = 'Registro sistema reservas'
    message = 'Se ha enviado una solicitud al Administrador.'
    from_email = settings.EMAIL_HOST
    to_list = [email, settings.EMAIL_HOST_USER]

    send_mail(subject, message, from_email, to_list, fail_silently=True)

    messages.success(request, 'SU REGISTRO HA SIDO PROCESADO CON EXITO')
    return

#**register**
"""
Verifica que el registro sea valido. De ser valido, crea el usuario
"""
def register(request):
    form = RegisterForm(request.POST) if request.method == 'POST' else RegisterForm()
    if request.method == 'POST' and form.is_valid():
        save_it=form.save(commit=False)
        save_it.save()


        subject = 'Registro sistema reservas'
        message = 'Su registro ha sido procesado con exito. \n Se ha enviado una solicitud al Administrador.\n Una vez que su solicitud halla sido procesada, se le notificara nuevamente. \n\n Saludos.\n\n El equipo de desarrollo.'
        from_email = settings.EMAIL_HOST
        to_list = [save_it.email, settings.EMAIL_HOST_USER]

        send_mail(subject, message, from_email, to_list, fail_silently=True)
        return HttpResponseRedirect('/login?newuser=true')

    context = {
        'form': form
    }
    return render(request, 'users/register.html', context)

#**search_users**
"""
Busca el usuario solicitado segun el criterio seleccionado
"""
def search_users(request):
    criteria = {}
    field = request.GET.get('field', None)
    if field is None:
        return Usuario.objects.all()
    criteria[field + "__iexact"] = request.GET.get('q', "")
    return Usuario.objects.filter(**criteria)

#**users_list**
"""
lista los usuarios
"""
@login_required(login_url='/login/')
def users_list(request):
    """
    """
    result = {
        'object_list': handle_GET(request, search_users),
    }
    return render(request, 'users/users_list.html', result)

#**asignar_roles**
"""
retorna el id de los roles seleccionados
"""
def asignar_roles(user):
    def __(request):

        get_id = lambda x: int(x.split('-')[-1:][0])
        keys = filter(lambda x: 'user-role' in x,  request.POST.keys())
        valid = [get_id(x) for x in keys]
        return valid
        # valid = [int(x.split('-')[-1:][0]) for x in keys]

    return __

#**administrar_rol_user**
"""
asigna roles a los usuarios
"""
@login_required(login_url='/login/')
def administrar_rol_user(request, pk,template_name='users/rol_admin.html'):
    user = get_object_or_404(Usuario, pk=pk)
    print (user)
    valid=handle_POST(request, asignar_roles(user))
    if valid is not None:
        user.groups.clear()
        for n in valid:
            g = Group.objects.get(pk=n)
            g.user_set.add(user)

        return redirect('/users/')
    lista_roles = Group.objects.all()
    data = {'object_list' : lista_roles, 'user': Usuario, 'user_groups': user.groups.all()}
    return render(request, template_name, data)


#**users_create**
"""
El administrador crea un usuario
"""
@login_required(login_url='/login/')
def users_create(request, template_name='users/user_form.html'):
    form = UserForm(request.POST or None)

    activo = False
    if request.POST.get('is_active') == 'on':
        activo = True

    if form.is_valid():
        Usuario.objects.create_user(
            request.POST['username'],
            email=request.POST['email'],
            password=request.POST['password'],
            first_name=request.POST['first_name'], last_name=request.POST['last_name'],
            ci=request.POST['ci'],
            telefono=request.POST['telefono'], direccion=request.POST['direccion'],
            categoria=request.POST['categoria'],
            is_active=activo
        )

        return redirect('users:user_list')
    return render(request, template_name, {'form': form})

#**users_update**
"""
Modifica los datos del usuario
"""
@login_required(login_url='/login/')
def users_update(request, pk, template_name='users/user_form.html'):
    usuario = get_object_or_404(Usuario, pk=pk)
    print('el usuario es: ', usuario)
    form = UserForm(request.POST or None, instance=usuario)
    if form.is_valid():
        form.save()
        return redirect('/users/')
    return render(request, template_name, {'form': form})

#**users_delete**
"""
Elimina el usuario seleccionado
"""
@login_required(login_url='/login/')
def users_delete(request, pk, template_name='users/user_confirm_delete.html'):
    usuario = get_object_or_404(Usuario, pk=pk)
    if request.method == 'POST':
        usuario.delete()
        return redirect('users:user_list')
    return render(request, template_name, {'object': usuario})

#**groups_list**
"""
lista los roles de usuario
"""
@login_required(login_url='/login/')
def groups_list(request, template_name='users/group_list.html'):
    grupos = Group.objects.all()
    data = {
        'object_list' : grupos
    }
    return render(request, template_name, data)


# TODO: falta cambiarle el nombre a los permisos, y definir que permisos se van a poder seleccionar
#**groups_create**
"""
El administrador crea un rol de usuario
"""
@login_required(login_url='/login/')
def groups_create(request, template_name='users/group_form.html'):
    form = GroupsForm(request.POST or None)
    if form.is_valid():
        form.save()
        return redirect('users:groups_list')
    return render(request, template_name, {'form': form})

#**groups_update**
"""
Modifica los datos del grupo de usuarios
"""
@login_required(login_url='/login/')
def groups_update(request, pk, template_name='users/group_form.html'):
    grupo = get_object_or_404(Group, pk=pk)
    form = GroupsForm(request.POST or None, instance=grupo)
    if form.is_valid():
        form.save()
        return redirect('users:groups_list')
    return render(request, template_name, {'form': form})

#**groups_delete**
"""
Elimina el grupo de usuarios seleccionado
"""
@login_required(login_url='/login/')
def groups_delete(request, pk, template_name='users/group_confirm_delete.html'):
    grupo = get_object_or_404(Group, pk=pk)
    if request.method == 'POST':
        grupo.delete()
        return redirect('users:groups_list')
    return render(request, template_name, {'object': grupo})

#**users_pending**
"""
Genera un listado de los usuarios pendientes de aprobacion
"""
@login_required(login_url='/login/')
def users_pending(request):
    result = {
        'object_list': Usuario.objects.filter(is_active=False),
    }
    return render(request, 'users/users_pending.html', result)


#**users_approve**
"""
Genera un listado de los usuarios aprobados
"""
def users_approve(request):
    result = {
        'object_list': Usuario.objects.filter(is_active=False),
    }
    return render(request, 'users/users_list.html', result)


#**inicio**
"""
Retorna la pagina de inicio
"""
@login_required(login_url='/login/')
def inicio(request):
    return render(request, 'inicio.html')




#**Class UsuarioForm**
"""
Formulario de usuarios y sus categorias
"""
class UsuarioForm(ModelForm):
    class Meta:
        model = Usuario
        fields = ['categoria']

#**categoria**
"""
Asigna una categoria al usuario
"""
def categoria(request, pk, template_name='users/usuario_categoria.html'):
    usuario = get_object_or_404(Usuario, pk=pk)
    form = UsuarioForm(request.POST or None, instance=usuario)
    if form.is_valid():
        if(usuario.categoria!=0):
            print(usuario.email)
            usuario.is_active=True
        form.save()

        subject = 'Bienvenido al sistena de reservas de recursos'
        message = 'Su solicitud ha sido confirmada.\nAhora puede logearse y empezar a utilizar el sistema.\n\nSaludos.\n\nEl equipo de desarrollo, '
        from_email = settings.EMAIL_HOST
        to_list = [usuario.email, settings.EMAIL_HOST_USER]

        send_mail(subject, message, from_email, to_list, fail_silently=True)

        return redirect('users:user_pending')
    return render(request, template_name, {'form':form})
