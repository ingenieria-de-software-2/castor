"""castor URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, includels
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib.auth import  views as auth_views
from .views import *


urlpatterns = [
    url(r'^login/$', auth_views.login,  {'template_name': 'users/login.html', }),
    url(r'^register/$', register),
    url(r'^users/$', users_list, name='user_list'),

    url(r'^users/pending/$', users_pending, name='user_pending'),
    url(r'^categoria/(?P<pk>\d+)$', categoria, name='usuario_categoria'),
    url(r'^users/roles/(?P<pk>[0-9]+)/$', administrar_rol_user , name='admin_rol'),
    url(r'^users/edit/(?P<pk>\d+)$', users_update, name='users_edit'),
    url(r'^users/approve/(?P<pk>\d+)$', users_approve, name='users_approve'),
    url(r'^users/new/$', users_create, name='users_new'),
    url(r'^users/delete/(?P<pk>\d+)$', users_delete, name='users_delete'),
    url(r'^users/inicio', inicio, name='users_inicio'),

    url(r'^password_reset/$', auth_views.password_reset, name='password_reset'),
    url(r'^password_reset/done/$', auth_views.password_reset_done, name='password_reset_done'),
    url(r'^reset/(?P<uidb64>[0-9A-Za-z_\-]+)/(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})/$',
        auth_views.password_reset_confirm, name='password_reset_confirm'),
    url(r'^reset/done/$', auth_views.password_reset_complete, name='password_reset_complete'),


    url(r'^login/users/inicio/$', inicio,name='users_inicio'),

    url(r'^roles/$', groups_list, name='groups_list'),
    url(r'^roles/new$', groups_create, name='groups_new'),
    url(r'^roles/edit/(?P<pk>\d+)$', groups_update, name='groups_edit'),
    url(r'^roles/delete/(?P<pk>\d+)$', groups_delete, name='groups_delete'),


]
