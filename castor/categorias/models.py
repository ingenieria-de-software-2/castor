from django.db import models


class Categoria(models.Model):
    nombre = models.CharField(max_length=200)
    orden = models.IntegerField()


    """
        Agrega soporte para comparar con el signo <
    """
    def __lt__(self, other):
        return self.orden < other.orden


    """
        Agrega soporte para comparar con el signo >
    """
    def __gt__(self, other):
        return self.orden > other.orden

    """
        Agrega soporte para comparar con el signo ==
    """
    def __eq__(self, other):
        return self.orden == other.orden

    """
        Retorna una representación en cadena
    """

    def __str__(self):

        return "Categoria: [%s : %i]" %(self.nombre, self.orden)
