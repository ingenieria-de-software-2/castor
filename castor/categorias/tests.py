from django.test import TestCase
from .models import Categoria
# Create your tests here.

# funcionario < alumno < auxiliar < encargado < asistente < adjunto < titular < admin-general

roles = [
    "funcionario",
    "alumno",
    "auxiliar",
    "encargado",
    "asistente",
    "adjunto",
    "titular",
    "admin-general"
]
# getRole :: String -> Categoria
def getRole(str):
    return Categoria.objects.filter(nombre=str).first()

class CategoryTestCase(TestCase):

    def test_alumno_funcionario(self):
        alumno = getRole("alumno")
        funcionario = getRole("funcionario")
        self.assertLess(funcionario, alumno, "error funcionario no es menor a alumno")

    def test_funcionario_es_menor(self):
        funcionario = getRole("funcionario")
        categoria = map(getRole, roles)
        menor = min(categoria)
        self.assertEquals(menor, funcionario, "error funcionario no es el menor de todos")

    def test_admin_mayor_es_menor(self):
        admin = getRole("admin-general")
        categoria = map(getRole, roles)
        mayor = max(categoria)
        self.assertEquals(mayor, admin, "error admin no es el mayor de todos los roles")
