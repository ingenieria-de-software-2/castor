from django.db import models
from users.models import Usuario
from recursos.models import Recurso
from datetime import datetime


#**Class Reserva**
"""
Crea el formulario de Reservas
"""
class Reserva(models.Model):
    usuario = models.ForeignKey(Usuario, null=False)
    recurso = models.ForeignKey(Recurso, null=False)
    # fecha_inicio = models.DateTimeField(null=False, default=datetime.now())
    # fecha_fin = models.DateTimeField(null=False, default=datetime.now())
    verificacion = models.IntegerField(null=True)